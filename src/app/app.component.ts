import { Component } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	//my_notes: FirebaseListObservable<any[]>;
	my_notes: any;

  title = 'app';
  name = '';

  constructor(public afDB: AngularFireDatabase) {
    if (navigator.onLine) {
	    this.getNotes()
	    	.subscribe(
	    		notas => {
	    			this.my_notes = notas;
	    			localStorage.setItem('my_notes',JSON.stringify(this.my_notes));
	    		}
	    	);
	    }else{
	    	this.my_notes = JSON.parse(localStorage.getItem('my_notes'));
	    }
  }

  getNotes(){
  	return this.afDB.list('/notas');
  }

  removeNote(){
  	this.afDB.database.ref('notas/' + this.note.id).remove();
  	this.show_form = false;		//Esconder formulario
	this.note = {id: null, title: null, description:null}; //Limpiar el arreglo nota  
	localStorage.setItem('my_notes',JSON.stringify(this.my_notes));  	
  }
  /*
  my_notes = [
  	{id:1, title: 'Note 1', description: 'Description for note 1'},
  	{id:2, title: 'Note 2', description: 'Description for note 2'},
  	{id:3, title: 'Note 3', description: 'Description for note 3'},
  	{id:4, title: 'Note 4', description: 'Description for note 4'},
  	{id:5, title: 'Note 5', description: 'Description for note 5'},
  ];*/
  note = {id: null, title: null, description:null};
  show_form = false;
  editing = false;
  addNote(){
  	this.show_form = true;
  }
  viewNote(note){
  	this.editing = true;
  	this.note = note;
  	this.show_form = true;
  }
  cancel(){
  	this.show_form = false;
  }
  /*
  delete(){
  	var me = this;
  	this.my_notes.forEach(function(el,i){
  		if(el == me.note){
  			me.my_notes.splice(i,1);
  		}
  	});

	  	me.show_form = false; //Ocultar formulario
	  	me.note = {id: null, title: null, description:null}; //Vaciar el arreglo 
	  	
  }*/
  createNote(){
  	if(this.editing){
  		this.afDB.database.ref('notas/' + this.note.id).set(this.note);
  		this.editing = false;
  	}else{
  		this.note.id = Date.now();
	  	this.afDB.database.ref('notas/' + this.note.id).set(this.note); 
  	}

	this.show_form = false;
	this.note = {id: null, title: null, description:null};  
	localStorage.setItem('my_notes',JSON.stringify(this.my_notes)); //Guardar la información en local

  	/*
  	if(this.editing){
  		//Editar
  		//alert("Editar: ");
  		var me = this;
  		this.my_notes.forEach(function(el,i){
  			if(el.id === me.note.id){
  				me.my_notes[i] = me.note
  			}
  		});

	  	me.show_form = false;
	  	me.note = {id: null, title: null, description:null};  
	  	
  	}else{
	 	this.note.id = Date.now();
	 	this.my_notes.push(this.note);
	  	this.show_form = false;
	  	this.note = {id: null, title: null, description:null};  
	  	
  	}*/
  }

}
