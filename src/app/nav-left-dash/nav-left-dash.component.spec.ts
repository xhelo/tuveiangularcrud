import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLeftDashComponent } from './nav-left-dash.component';

describe('NavLeftDashComponent', () => {
  let component: NavLeftDashComponent;
  let fixture: ComponentFixture<NavLeftDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavLeftDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavLeftDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
