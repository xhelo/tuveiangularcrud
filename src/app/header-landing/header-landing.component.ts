import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-landing',
  templateUrl: './header-landing.component.html',
  styleUrls: ['./header-landing.component.css', '../../assets/css/style.css']
})
export class HeaderLandingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
