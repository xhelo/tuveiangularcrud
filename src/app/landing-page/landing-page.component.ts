import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  // tslint:disable-next-line:max-line-length
  styleUrls: ['./landing-page.component.css', '../../assets/css/bootstrap.css', '../../assets/css/bootstrap-responsive.css', '../../assets/css/default.css', '../../assets/css/style.css']
})
export class LandingPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
