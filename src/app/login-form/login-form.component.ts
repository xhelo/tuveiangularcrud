import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  loginUser(e){
    e.preventDefault();
    console.log(e);
    var username = e.target.elements[1].value;
    var password = e.target.elements[2].value;
    console.log(username, password);

    if (username == 'admin' && password == 'admin') {
      this.router.navigate(['dashboard']);   
    }
  }

  loginGoogle(){
     console.log('Login google');
  }

  /** LISTA DE FORMULARIOS VISIBLES */
  formIngresar = true;
  formRecuperar = false;
  formRegistro = false;

  showRecovery(){
    this.formIngresar = false;
    this.formRecuperar = true;
    this.formRegistro = false;
  }
  showLogin(){
    this.formIngresar = true;
    this.formRecuperar = false;
    this.formRegistro = false;
  }
  showRegister(){
    this.formIngresar = false;
    this.formRecuperar = false;
    this.formRegistro = true;
  }
}
