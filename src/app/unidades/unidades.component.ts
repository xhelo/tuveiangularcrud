import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database'; 

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit {

  // my_notes: FirebaseListObservable<any[]>;
  my_unidades: any;

  title = 'app';
  name = '';
  accion = '';

  constructor(public afDB: AngularFireDatabase) {
    if (navigator.onLine) {
        this.getUnidades()
	    	.subscribe(
	    		unidades => {
	    			this.my_unidades = unidades;
	    			localStorage.setItem('my_unidades', JSON.stringify(this.my_unidades));
	    		}
        );
	    }else{
	    	this.my_unidades = JSON.parse(localStorage.getItem('my_unidades'));
	    }
  }

  getUnidades(){
    return this.afDB.list('/unidades');
  }

  // tslint:disable-next-line:member-ordering
  /*my_unidades = [
  	// tslint:disable-next-line:indent
  	{id: 1, concesionado: 'Yo', modelo: 'Modelo'},
  	{id: 2, concesionado: 'Chelo', modelo: 'Jetta'},
  	{id: 3, concesionado: 'Marcelo', modelo: 'Ford'},
  	{id: 4, concesionado: 'Marc', modelo: 'Autor'},
  ];*/

  removeUnidad(){
  	this.afDB.database.ref('unidades/' + this.unidad.id).remove();
  	this.show_form = false;		// Esconder formulario
	this.unidad = {id: null, concesionado: null, modelo: null, numEconomico: null, tipo: null}; // Limpiar el arreglo unidad
	localStorage.setItem('my_unidades', JSON.stringify(this.my_unidades));
  }

  unidad = {id: null, concesionado: null, modelo: null, numEconomico: null, tipo: null};
  show_form = false;
  editing = false;
  btnEliminar = false;
  btnGuardar = false;

  /*addNote(){
  	this.show_form = true;
  }*/

  addUnidad(){
    this.accion = "Agregar ";
    this.unidad = {id: null, concesionado: null, modelo: null, numEconomico: null, tipo: null};
    this.show_form = true;
    this.editing = false;
    this.btnGuardar = true;
    this.btnEliminar = false;
  }

  viewUnidad(unidad){
  	this.editing = true;
    this.unidad = unidad;
    this.accion = "Editar ";
    this.show_form = true;
    this.btnGuardar = true;
    this.btnEliminar = false;
  }


  viewUnidadDelete(unidad){
  	//this.editing = true;
    this.unidad = unidad;
    this.accion = "Eliminar ";
    this.show_form = true;
    this.btnGuardar = false;
    this.btnEliminar = true;
  }

  cancel(){
  	this.show_form = false;
    this.unidad = {id: null, concesionado: null, modelo: null, numEconomico: null, tipo: null};
  }
  /*
  delete(){
  	var me = this;
  	this.my_notes.forEach(function(el,i){
  		if(el == me.note){
  			me.my_notes.splice(i,1);
  		}
  	});

	  	me.show_form = false; //Ocultar formulario
	  	me.note = {id: null, title: null, description:null}; //Vaciar el arreglo

  }*/
  createUnidad(){
  	if (this.editing){
  		this.afDB.database.ref('unidades/' + this.unidad.id).set(this.unidad);
  		this.editing = false;
  	}else{
  		this.unidad.id = Date.now();
	  	this.afDB.database.ref('unidades/' + this.unidad.id).set(this.unidad);
  	}

  this.show_form = false;
	this.unidad = {id: null, concesionado: null, modelo: null, numEconomico: null, tipo: null};
	localStorage.setItem('my_unidades', JSON.stringify(this.my_unidades)); // Guardar la información en local
  }

  ngOnInit() {
  }

}
