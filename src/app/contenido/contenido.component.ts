import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() mostrar;
  
    ngOnChanges(changes) {
      if (changes['mostrar']) {
        // Aquí ya sabes que has recibido un nuevo dato desde cualquier componente.
        const nuevoMostrar = changes.mostrar;
      }
    }

    @Input() nuevoMostrar: string;
}
