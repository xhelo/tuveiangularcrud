import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database'; 

@Component({
  selector: 'app-conductores',
  templateUrl: './conductores.component.html',
  styleUrls: ['./conductores.component.css']
})
export class ConductoresComponent implements OnInit {

  // my_notes: FirebaseListObservable<any[]>;
  my_conductores: any;
  
    title = 'app';
    name = '';
    accion = '';
  
    constructor(public afDB: AngularFireDatabase) {
        if (navigator.onLine) {
          this.getConductores()
          .subscribe(
            conductores => {
              this.my_conductores = conductores;
              localStorage.setItem('my_conductores', JSON.stringify(this.my_conductores));
            }
          );
        }else{
          this.my_conductores = JSON.parse(localStorage.getItem('my_conductores'));
        }
    }

    getConductores(){
      return this.afDB.list('/conductores');
    }

    removeConductor(){
      this.afDB.database.ref('conductores/' + this.conductor.id).remove();
      this.show_form = false;		// Esconder formulario
    this.conductor = {id: null, activo: null, email: null, licencia: null, nombre: null, telefono: null}; // Limpiar el arreglo unidad
    localStorage.setItem('my_conductores', JSON.stringify(this.my_conductores));
    }
  
    conductor = {id: null, activo: null, email: null, licencia: null, nombre: null, telefono: null}; // Limpiar el arreglo unidad
    show_form = false;
    editing = false;
    btnEliminar = false;
    btnGuardar = false;
  
    /*addNote(){
      this.show_form = true;
    }*/

    addConductor(){
      this.accion = "Agregar ";
      this.conductor = {id: null, activo: null, email: null, licencia: null, nombre: null, telefono: null}; // Limpiar el arreglo unidad
      this.show_form = true;
      this.editing = false;
      this.btnGuardar = true;
      this.btnEliminar = false;
    }

    viewConductor(conductor){
      this.editing = true;
      this.conductor = conductor;
      this.accion = "Editar ";
      this.show_form = true;
      this.btnGuardar = true;
      this.btnEliminar = false;
    }

    viewConductorDelete(conductor){
      //this.editing = true;
      this.conductor = conductor;
      this.accion = "Eliminar ";
      this.show_form = true;
      this.btnGuardar = false;
      this.btnEliminar = true;
    }
  
    cancel(){
      this.show_form = false;
      this.conductor = {id: null, activo: null, email: null, licencia: null, nombre: null, telefono: null}; // Limpiar el arreglo unidad
    }
    
    createConductor(){
      if (this.editing){
        this.afDB.database.ref('conductores/' + this.conductor.id).set(this.conductor);
        this.editing = false;
      }else{
        this.conductor.id = Date.now();
        this.afDB.database.ref('conductores/' + this.conductor.id).set(this.conductor);
      }
  
    this.show_form = false;
    this.conductor = {id: null, activo: null, email: null, licencia: null, nombre: null, telefono: null}; // Limpiar el arreglo unidad
    localStorage.setItem('my_conductores', JSON.stringify(this.my_conductores)); // Guardar la información en local
    }
  
    ngOnInit() {
    }

}
