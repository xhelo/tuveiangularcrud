import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { HeaderDashComponent } from './header-dash/header-dash.component';
import { FooterDashComponent } from './footer-dash/footer-dash.component';
import { NavLeftDashComponent } from './nav-left-dash/nav-left-dash.component';
import { ConductoresComponent } from './conductores/conductores.component';
import { HeaderLandingComponent } from './header-landing/header-landing.component';
import { ContenidoComponent } from './contenido/contenido.component';
import { RecepcionistasComponent } from './recepcionistas/recepcionistas.component';

const appRoutes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'loginForm',
    component: LoginFormComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'unidades',
    component: UnidadesComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    UnidadesComponent,
    LoginFormComponent,
    DashboardComponent,
    LandingPageComponent,
    HeaderDashComponent,
    FooterDashComponent,
    NavLeftDashComponent,
    ConductoresComponent,
    HeaderLandingComponent,
    ContenidoComponent,
    RecepcionistasComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
  	FormsModule,
  	AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

